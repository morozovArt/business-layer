import os, sys
import socket
import threading

from singleton import ClientForScene, CommonMessage

# config
host = ''
size = 1024
port_rca = 9099
port_3d_scene = 9093
robo_dict = {'f': ' ', 't': ' '}
# config end

#TODO 1. Фиксировать изменения сообщения роботам, чтобы отправлять только при изменении, а не спамить
#TODO 2. Проблема перезаписывания планером сообщения до отправки роботу
#TODO 3. Рефакторинг
#TODO 4. Логирование


def listen_to_robot(client, who, client_for_scene, common_message):

    while True:

        if common_message.messages.get(who) == 'e':  # dict.get() may require a lot of time | to backlog
            sys.exit(0)

        if common_message.get_messages().get(who) == 'none':
            continue

        client.send(common_message.get_messages().get(who).encode())
        data = client.recv(size)

        if data:
            client_for_scene.send(data)
        else:
            raise socket.error('Client disconnected')



def listen_to_planner(client, common_message):
    while True:

        data = client.recv(size)
        message = data.decode()

        if data:

            if message == 'e':
                os._exit(0)

            common_message.set_message(message)

        else:
            raise socket.error('Client disconnected')


common_message = CommonMessage(robo_dict)
client_for_scene = ClientForScene(port_3d_scene)
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.bind((host, port_rca))
sock.listen(5)

while True:
    client, address = sock.accept()
    who = client.recv(size).decode()
    if who != 'p':

        threading.Thread(target = listen_to_robot, args = (client, who, client_for_scene, common_message)).start()
    else:
        threading.Thread(target = listen_to_planner, args=(client, common_message)).start()


