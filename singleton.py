import socket
from wrapt import synchronized


@synchronized
class ClientForScene:

    def __init__(self, port):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect(('', port))
        self.socket.send(b'RCA')

    @synchronized
    def send(self, message):
        self.socket.send(message)


@synchronized
class CommonMessage:

    def __init__(self, robo_dict):
        self.messages = {}
        self.messages.update(robo_dict)

    @synchronized
    def set_message(self, message):
        cmd = message.split(':')
        self.messages.update({cmd[0]:cmd[1]})

    @synchronized
    def get_messages(self):
        return self.messages